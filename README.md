My personal KiCad library. Anyone can use the symbols/footprint freely.
Note that the 3D models which I downloaded from the manufacturers website are under the license from that manufacturer. Refer to their website for more information.
